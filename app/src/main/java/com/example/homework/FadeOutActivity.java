package com.example.homework;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class FadeOutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fade_out);
        getSupportActionBar().setTitle("To do");
        ImageView imageView = findViewById(R.id.img_fade_in);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.fade_out);
        imageView.startAnimation(animation);
    }
}