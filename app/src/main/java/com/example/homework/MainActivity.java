package com.example.homework;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Intent intent = new Intent();
    View myView;
    boolean up;

    EditText title, description;
    Button btnCancel,btnSave;

    TextView textView;

    ListView listView;

    List<Noted> content  = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        myView = findViewById(R.id.my_view);
        myView.setVisibility(View.INVISIBLE);
        up = false;


        btnCancel = findViewById(R.id.btn_cancel);
        btnSave = findViewById(R.id.btn_save);

        btnCancel.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        textView = findViewById(R.id.contact_view);
        title = findViewById(R.id.txt_title);
        description = findViewById(R.id.txt_decs);
        listView = findViewById(R.id.list_view);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.homework,menu);
        getSupportActionBar().setTitle("To do");
        return true;
    }

    @SuppressLint("ResourceType")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        switch (item.getItemId()) {
            case R.id.add:
                onSlideViewButtonClick(myView);
                return true;
            case R.id.fade_in:
                intent.setClass(this,FadeInActivity.class);
                startActivity(intent);
                return true;
            case R.id.fade_out:
                intent.setClass(this,FadeOutActivity.class);
                startActivity(intent);
                return true;
            case R.id.zoom:
                intent.setClass(this,ZoomActivity.class);
                startActivity(intent);
                return true;
            case R.id.rotate:
                intent.setClass(this,RotateActivity.class);
                startActivity(intent);
                return true;
            case R.id.remove_take:
                RemoveTake();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }



    public void slideUp(View view){

        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(1000);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setTranslationY(240);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(1000);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setTranslationY(1300);
    }

    public  void onSlideViewButtonClick(View view){
        if (up) {
            slideDown(myView);
        } else {
            slideUp(myView);
        }
        up = !up;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cancel:
                onSlideViewButtonClick(v);
                break;
            case R.id.btn_save:

                String Title = title.getText().toString();
                String Description = description.getText().toString();

                if(Title.isEmpty()){
                    Toast.makeText(this,"Enter Title",Toast.LENGTH_SHORT).show();
                }else if(Description.isEmpty()){
                    Toast.makeText(this,"Enter Description",Toast.LENGTH_SHORT).show();
                }else{
                    AddNew(Title,Description);
                    if(content.size() > 0){
                        textView.setVisibility(v.GONE);
                    }else{
                        textView.setVisibility(v.VISIBLE);
                    }
                    title.setText("");
                    description.setText("");
                }
                break;
        }
    }

    public void AddNew(String Title,String Description){

        content.add(new Noted(Title,Description));
        CustomAdapter arrayAdapter = new CustomAdapter(this,content);
        listView.setAdapter(arrayAdapter);


    }

    private void RemoveTake() {
        content.removeAll(content);
        textView.setVisibility(myView.VISIBLE);
        CustomAdapter arrayAdapter = new CustomAdapter(this,content);
        listView.setAdapter(arrayAdapter);
    }
}