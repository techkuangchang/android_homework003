package com.example.homework;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

public class CustomAdapter extends BaseAdapter {

    Context context;
    List<Noted> noted;
    LayoutInflater inflater;

    public CustomAdapter(Context context, List<Noted> noted) {
        this.context = context;
        this.noted = noted;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return noted.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView =inflater.inflate(R.layout.list_custom_view,null);
        TextView head = convertView.findViewById(R.id.title_header);
        TextView body = convertView.findViewById(R.id.title_body);

        head.setText(noted.get(position).getTitle());
        body.setText(noted.get(position).getDescription());

        return convertView;
    }
}
